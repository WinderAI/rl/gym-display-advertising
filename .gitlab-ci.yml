image: "python:3.7"

# Change pip's cache directory to be inside the project directory since we can
# only cache local items.
variables:
  PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"

# Pip's cache doesn't store the python packages
# https://pip.pypa.io/en/stable/reference/pip_install/#caching
#
# If you want to also cache the installed packages, you have to install
# them in a virtualenv and cache it as well.
cache:
  paths:
    - .cache/pip
    - venv/

before_script:
  - python -V # Print out python version for debugging
  - pip install virtualenv
  - virtualenv venv
  - source venv/bin/activate
  - pip install -r requirements.txt

stages:
  - Static Analysis
  - Unit Test
  - Integration Test
  - Deploy

linting:
  stage: Static Analysis
  script:
    - black --check gym_display_advertising
    - black --check tests

unit_test:
  stage: Unit Test
  script:
    - pytest

build_test:
  stage: Unit Test
  script:
    - python3 setup.py sdist bdist_wheel
    - twine check dist/*

py37:
  stage: Integration Test
  image: "python:3.7"
  script:
    - tox -e py37

py36:
  stage: Integration Test
  image: "python:3.6"
  script:
    - tox -e py36

py3:
  stage: Integration Test
  image: "python:3.7"
  script:
    - tox -e py3

deploy_test:
  stage: Deploy
  variables:
    TWINE_USERNAME: $TEST_USERNAME
    TWINE_PASSWORD: $TEST_PASSWORD
  script:
    - python3 setup.py sdist bdist_wheel
    - twine upload --verbose --repository-url https://test.pypi.org/legacy/ dist/*
    - pip install --extra-index-url https://pypi.org/simple -i https://test.pypi.org/simple/ gym-display-advertising==$CI_JOB_ID
  except:
    - tags

deploy_production:
  stage: Deploy
  variables:
    TWINE_USERNAME: $PRODUCTION_USERNAME
    TWINE_PASSWORD: $PRODUCTION_PASSWORD
  script:
    - python3 setup.py sdist bdist_wheel
    - twine upload dist/*
  only:
    - tags
